import {Logger} from "./util/logger";
import {Console} from "./util/console";
import {BotInitializer} from "./lamabot/botinitializer";
import {_} from "./util/util";

declare var process:any;
declare function require(name:string):any;

Logger.initialize("latest.log", "error.log");

process.on('uncaughtException', (err:Error) => {
    Logger.err("Uncaught program error: " + err.message);
    Logger.err(err.stack);
});

Console.initialize();

const config = require("./config.json");
const fs = require("fs");

let initializers = [];
let files = fs.readdirSync("./");

for(let i = 0; i < files.length; i++) {
    if(_.startsWith(files[i], "conf_")) {
        let cfgfolder = "./" + files[i];
        let lstat = fs.lstatSync(cfgfolder);
        if(lstat.isDirectory()) {
            initializers.push(new BotInitializer(cfgfolder, config));
        }
    }
}

for(let i = 0; i < initializers.length; i++) {
    initializers[i].initialize();
    initializers[i].start(() => {});
}