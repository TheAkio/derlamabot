export class _ {

    public static startsWith(string:string, prefix:string) {
        return string.slice(0, prefix.length) === prefix;
    }

    public static contains(string:string, contains:string) {
        return string.indexOf(contains) > -1;
    }

    public static equals(string1:string, string2:string) {
        return string1 == string2;
    }

    public static containsIgnoreCase(string:string, contains:string) {
        return string.toLowerCase().indexOf(contains.toLowerCase()) > -1;
    }

    public static equalsIgnoreCase(string1:string, string2:string) {
        return string1.toLowerCase() == string2.toLowerCase();
    }

}