import {Logger} from "./logger";

declare var process:any;
declare function require(name:string):any;

var readline = require('readline');

export class Console {

    public static initialize() {
        var rl_interface = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        });

        rl_interface.on('line', (input:string) => {
            var split = input.split(" ");
            var cmd = split[0];
            var args = [];
            var i = 1;
            for(i; i < split.length; i++) {
                args[i - 1] = split[i];
            }

            if(cmd === "help" || cmd === "?") {
                Logger.log("[C] Nothing here!");
            }
        });
    }

}