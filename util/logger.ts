declare function require(name:string):any;

const filesystem = require('fs');

export class Logger {

    private static logger:Logger = null;

    private logFile:string;
    private errorLogFile:string;

    private constructor(logFileName:string, errorLogFileName:string) {
        if(Logger.logger !== null) {
            Logger.log("Logger instance has already been initialized!");
        }
        else {
            this.logFile = "./" + logFileName;
            this.errorLogFile = "./" + errorLogFileName;

            if(!filesystem.existsSync(this.logFile)) {
                filesystem.writeFileSync(this.logFile, "");
            }

            if(!filesystem.existsSync(this.errorLogFile)) {
                filesystem.writeFileSync(this.errorLogFile, "");
            }

            Logger.logger = this;
        }
    }

    public static initialize(logFileName:string, errorLogFileName:string) {
        new Logger(logFileName, errorLogFileName);
    }

    private static prepareMessage(message:string) {
        let date = new Date();

        let day = date.getDate().toString();
        let month = (date.getMonth() + 1).toString();
        let hours = date.getHours().toString();
        let minutes = date.getMinutes().toString();
        let seconds = date.getSeconds().toString();

        day = day.length == 1 ? "0" + day : day;
        month = month.length == 1 ? "0" + month : month;
        hours = hours.length == 1 ? "0" + hours : hours;
        minutes = minutes.length == 1 ? "0" + minutes : minutes;
        seconds = seconds.length == 1 ? "0" + seconds : seconds;

        let timeString = day + "/" + month + "/" + date.getFullYear() + " " + hours + ":" + minutes + ":" + seconds;
        return "[" + timeString + "] " + message;
    }

    public static log(message:string) {
        message = Logger.prepareMessage(message);
        console.log(message);
        if(Logger.logger !== null) {
            filesystem.appendFile(Logger.logger.logFile, message + "\n");
        }
    }

    public static err(message:string) {
        message = Logger.prepareMessage("ERROR: " + message);
        console.error(message);
        if(Logger.logger !== null) {
            filesystem.appendFile(Logger.logger.logFile, message + "\n");
            filesystem.appendFile(Logger.logger.errorLogFile, message + "\n");
        }
    }

    public static warn(message:string) {
        message = Logger.prepareMessage("WARNING: " + message);
        console.log(message);
        if(Logger.logger !== null) {
            filesystem.appendFile(Logger.logger.logFile, message + "\n");
            filesystem.appendFile(Logger.logger.errorLogFile, message + "\n");
        }
    }

}