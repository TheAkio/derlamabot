import {GoogleOAuth} from "./oauth";
import {Logger} from "../util/logger";

declare function require(name:string):any;

const google = require('googleapis');
const youtube = google.youtube('v3');

export abstract class YouTubeChatPoll {

    private lamaBotAuth:GoogleOAuth;
    private userAuth:GoogleOAuth;
    private liveChatId:string;
    private polling:boolean = false;
    private pageToken:string;
    private recvFirstPoll:boolean = false;

    constructor(lamaBotAuth:GoogleOAuth, userAuth:GoogleOAuth) {
        this.lamaBotAuth = lamaBotAuth;
        this.userAuth = userAuth;
    }

    public startPolling() {
        if(this.polling) return;
        this.polling = true;
        Logger.log("Loading YouTube Chat ID...");
        this.getChatId(() => {
            if(this.liveChatId != undefined) {
                if(this.polling) {
                    Logger.log("Polling started!");
                    this.poll();
                }
            }
            else {
                if(this.polling) {
                    setTimeout(() => {
                        this.stopPolling();
                        this.startPolling();
                    }, 10000);
                }
            }
        });
    }

    private poll() {
        let pollingInterval = 10000;
        let params:any = {liveChatId:this.liveChatId,part:"id,snippet,authorDetails",auth: this.lamaBotAuth.getClient()};
        if(this.pageToken != undefined) params.pageToken = this.pageToken;
        youtube.liveChatMessages.list(params, (err:any, data:any) => {
            if(!err) {
                this.pageToken = data.nextPageToken;
                pollingInterval = data.pollingIntervalMillis;
                if(this.polling) {
                    setTimeout(() => {
                        if(this.polling) this.poll();
                    }, pollingInterval);
                }
                if(!this.recvFirstPoll) this.recvFirstPoll = true;
                else {
                    for(let i = 0; i < data.items.length; i++) {
                        this.onYouTubeMessage(data.items[i]);
                    }
                }
            }
            else {
                Logger.err("YT ERROR: " + err);
                setTimeout(() => {
                        if(this.polling) {
                            this.stopPolling();
                            this.startPolling();
                        }
                }, pollingInterval);
            }
        });
    }

    public stopPolling() {
        this.polling = false;
        this.liveChatId = undefined;
        this.pageToken = undefined;
    }

    private getChatId(callback:() => void) {
        youtube.liveBroadcasts.list({part:"snippet", mine:true, broadcastType:"all", auth: this.userAuth.getClient()}, (err:any, data:any) => {
            if(err) {
                Logger.err("YT ERROR: " + err);
            }
            else {
                if(data != undefined && data.items != undefined && data.items[0] != undefined && data.items[0].snippet.liveChatId != undefined) {
                    let item = data.items[0];
                    this.liveChatId = item.snippet.liveChatId;
                }
                else {
                    Logger.err("YT ERROR: Live chat ID not found!");
                }
            }
            callback();
        });
    }

    public insert(message:string, callback:() => void) {
        if(this.liveChatId == undefined) {
            Logger.err("YT ERROR: Failed to send message, no liveChatId defined!");
            return;
        }

        youtube.liveChatMessages.insert({auth: this.lamaBotAuth.getClient(), part:"snippet", resource:{snippet:{textMessageDetails: {messageText: message},liveChatId: this.liveChatId,type: "textMessageEvent"}}}, (err:any, data:any) => {
            if(err) {
                Logger.err("YT ERROR: " + err);
            }
            callback();
        });
    }

    protected abstract onYouTubeMessage(data:any):void;

}