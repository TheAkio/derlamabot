declare function require(name:string):any;

const google = require('googleapis');
const OAuth2 = google.auth.OAuth2;
const youtube = google.youtube('v3');
const config = require("../config.json");

const channelOwnerScopes = [
    "https://www.googleapis.com/auth/youtube.readonly",
    "https://www.googleapis.com/auth/youtube",
    "https://www.googleapis.com/auth/youtube.force-ssl"
]

export class GoogleOAuth {

    private tokens:any;

    private oauth2Client = new OAuth2(
        config.youtubeApp.client_id,
        config.youtubeApp.client_secret,
        config.youtubeApp.redirect_uris[0]
    );

    public generateURL() {
        return this.oauth2Client.generateAuthUrl({
            access_type: "offline",
            scope: channelOwnerScopes,
            prompt: "consent"
        });
    }

    public setCredentials(code:string, callback:(err:any) => void) {
        this.oauth2Client.getToken(code, (err:any, tokens:any) => {
            if(!err) {
                this.setAcccessToken(tokens);
                callback(undefined);
            }
            else {
                callback(err);
            }
        });
    }

    public setAcccessToken(tokens:any) {
        this.tokens = tokens;
        this.oauth2Client.setCredentials(tokens);
    }

    public refreshAccessToken() {
        this.oauth2Client.refreshAccessToken((err:any, tokens:any) => {
            if(err) {
                console.log(err);
            }
            else {
                this.tokens = tokens;
            }
        });
    }

    public getTokens() {
        return this.tokens;
    }

    public getClient() {
        return this.oauth2Client;
    }

}