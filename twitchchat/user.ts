import {_} from "../util/util";

export class TwitchUser {

    private badges:string;
    private color:string;
    private displayName:string;
    private emotes:string;
    private id:string;
    private mod:boolean;
    private roomId:number;
    private sentTs:number;
    private subscriber:boolean;
    private tmiSentTs:number;
    private turbo:boolean;
    private userId:number;
    private userType:string;

    constructor(rawCommand:string) {
        let commandArgs = rawCommand.split(";");
        for(let i = 0; i < commandArgs.length; i++) {
            let arg = commandArgs[i];
            if(_.startsWith(arg, "@badges") || _.startsWith(arg, "badges")) {
                this.badges = arg.split("=")[1];
            }
            else if(_.startsWith(arg, "color")) {
                this.color = arg.split("=")[1];
            }
            else if(_.startsWith(arg, "display-name")) {
                this.displayName = arg.split("=")[1];
            }
            else if(_.startsWith(arg, "emotes")) {
                this.emotes = arg.split("=")[1];
            }
            else if(_.startsWith(arg, "id")) {
                this.id = arg.split("=")[1];
            }
            else if(_.startsWith(arg, "mod")) {
                this.mod = arg.split("=")[1] == "1" ? true : false;
            }
            else if(_.startsWith(arg, "room-id")) {
                this.roomId = parseInt(arg.split("=")[1], 10);
            }
            else if(_.startsWith(arg, "sent-ts")) {
                this.sentTs = parseInt(arg.split("=")[1], 10);
            }
            else if(_.startsWith(arg, "subscriber")) {
                this.subscriber = arg.split("=")[1] == "1" ? true : false;
            }
            else if(_.startsWith(arg, "tmi-sent-ts")) {
                this.tmiSentTs = parseInt(arg.split("=")[1], 10);
            }
            else if(_.startsWith(arg, "turbo")) {
                this.turbo = arg.split("=")[1] == "1" ? true : false;
            }
            else if(_.startsWith(arg, "user-id")) {
                this.userId = parseInt(arg.split("=")[1], 10);
            }
            else if(_.startsWith(arg, "user-type")) {
                this.userType = arg.split("=")[1];
            }
        }
    }

    public getBadges() {
        return this.badges;
    }

    public getColor() {
        return this.color;
    }

    public getDisplayName() {
        return this.displayName;
    }

    public getEmotes() {
        return this.emotes;
    }

    public getId() {
        return this.id;
    }

    public isMod() {
        return this.mod;
    }

    public getRoomId() {
        return this.roomId;
    }

    public getSentTs() {
        return this.sentTs;
    }

    public isSubscriber() {
        return this.subscriber;
    }

    public getTmiSentTs() {
        return this.tmiSentTs;
    }

    public isTurbo() {
        return this.isTurbo;
    }

    public getUserId() {
        return this.userId;
    }

    public getUserType() {
        return this.userType;
    }

}