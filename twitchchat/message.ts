export class TwitchMessage {

    private username:string;
    private channel:string;
    private message:string;
    private type:string;

    constructor(rawArgument:string) {
        this.type = rawArgument.split(" ")[1];
        if(this.type == "PRIVMSG") {
            this.username = rawArgument.split("!")[0];
            this.channel = "#" + rawArgument.split("#")[1].split(":")[0].trim();
            this.message = rawArgument.split(":")[1];
        }
    }

    public getUsername() {
        return this.username;
    }

    public getChannel() {
        return this.channel;
    }

    public getMessage() {
        return this.message;
    }

    public getType() {
        return this.type;
    }

}