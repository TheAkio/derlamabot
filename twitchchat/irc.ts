import {Logger} from "../util/logger";
import {TwitchUser} from "./user";
import {TwitchMessage} from "./message";

declare function require(name:string):any;

const irc = require('irc');

export abstract class TwitchIRC {
    
    private ircClient:any;
    private username:string;
    private connectCallback:() => void;

    constructor(username:string, password:string, channels:string[]) {
        this.username = username;
        this.ircClient = new irc.Client("irc.twitch.tv", username, {
            autoConnect: false,
            autoRejoin: true,
            channels: channels,
            sasl: true,
            nick: username,
            userName: username.toLowerCase(),
            password: password
        });

        this.ircClient.addListener('registered', (message:string) => {
            Logger.log("Connected to IRC server!");
            setInterval(() => {
                this.ircClient.send('PONG', 'empty');
            }, 60 * 1000);
            this.ircClient.send("CAP REQ", 'twitch.tv/commands twitch.tv/tags twitch.tv/membership');
            this.connectCallback();
            this.connectCallback = undefined;
        });


        this.ircClient.addListener('raw', (message:{command:string,rawCommand:string,commandType:string,args:[string]}) => {
            if(message.args == undefined || message.args.length == 0) return;
            let tMessage = new TwitchMessage(message.args[0]);
            if(tMessage.getType() == "PRIVMSG") {
                let user = new TwitchUser(message.rawCommand);
                this.onTwitchMessage(user, tMessage);
            }
        });

        this.ircClient.addListener('error', (message:string) => {
            Logger.err("Twitch IRC: " + message);
        });
    }

    public connect(callback:() => void) {
        this.connectCallback = callback;
        this.ircClient.connect(2);
    }

    public disconnect(callback:() => void) {
        this.ircClient.disconnect(() => {
            Logger.log("Disconnected from IRC server!");
            callback();
        });
    }

    public say(channel:string, message:string) {
        this.ircClient.say(channel, message);
    }

    public getUsername() {
        return this.username;
    }

    protected abstract onTwitchMessage(user:TwitchUser, message:TwitchMessage):void;

}