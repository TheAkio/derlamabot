import {TwitchWrapper} from "./twitchwrapper";
import {YouTubeWrapper} from "./youtubewrapper";
import {LamaClient} from "./lamaclient";
import {LamaBot} from "./lamabot";
import {Logger} from "../util/logger";
import {GoogleOAuth} from "../youtubechat/oauth";
import {YouTubeChatPoll} from "../youtubechat/chatpoll";

declare function require(name:string):any;

const fs = require("fs");
const config = require("../config.json");

export class BotInitializer {

    private globalConfig:any;
    private cfgfolder:string;
    private generalConfig:any;

    private twitchClient:TwitchWrapper;
    private twitchBot:LamaBot;
    private modBot:TwitchWrapper;

    private youtubeClient:YouTubeWrapper;
    private youtubeBot:LamaBot;

    constructor(cfgfolder:string, config:any) {
        this.cfgfolder = cfgfolder;
        this.globalConfig = config;
    }

    public initialize() {
        if(fs.existsSync(this.cfgfolder + "/general.json")) {
            this.generalConfig = require("../" + this.cfgfolder + "/general.json");
            if(this.generalConfig.twitch != undefined && this.generalConfig.twitch.channel != undefined) {
                Logger.log("Initializing Twitch Bot for " + this.cfgfolder);
                this.twitchClient = new TwitchWrapper(this.globalConfig.twitch.username, this.globalConfig.twitch.password, this.generalConfig.twitch.channel);
                this.twitchBot = new LamaBot(this.twitchClient, this.cfgfolder, this.generalConfig);
                if(this.generalConfig.modCommand != undefined) {
                    Logger.log("Initializing Twitch ModBot for " + this.cfgfolder);
                    this.modBot = new TwitchWrapper(this.generalConfig.modCommand.username, this.generalConfig.modCommand.password, "#" + this.generalConfig.modCommand.username.toLowerCase());
                    this.modBot.setMessageCallback((senderDisplayName:string, senderId:string, isMod:boolean, isOwner:boolean, message:string) => {
                        if(this.twitchBot.isRunning()) {
                            if((this.twitchBot.isModPlus(senderId) && isMod || isOwner)) {
                                let split = message.split(" ");
                                let cmd = split[0].toLowerCase();
                                if(cmd == "!mod" && split.length > 1) {
                                    let who = split[1].toLowerCase();
                                    this.modBot.say(".mod " + who);
                                }
                                else if(cmd == "!unmod" && split.length > 1) {
                                    let who = split[1].toLowerCase();
                                    if(!this.twitchBot.isModPlus(who)) this.modBot.say(".unmod " + who);
                                }
                            }
                        }
                    });
                }
            }
            if(this.generalConfig.youtube != undefined && this.generalConfig.youtube.token != undefined)  {
                Logger.log("Initializing YouTube Bot for " + this.cfgfolder);
                let lamaBotAuth = new GoogleOAuth();
                lamaBotAuth.setAcccessToken(config.youtubeToken);
                lamaBotAuth.refreshAccessToken();
                let userAuth = new GoogleOAuth();
                userAuth.setAcccessToken(this.generalConfig.youtube.token);
                userAuth.refreshAccessToken();
                this.youtubeClient = new YouTubeWrapper(lamaBotAuth, userAuth);
                this.youtubeBot = new LamaBot(this.youtubeClient, this.cfgfolder, this.generalConfig);
            }
        }
        else {
            Logger.err("General config of " + this.cfgfolder + " could not be found!");
        }
    }

    public start(callback:() => void) {
        if(this.generalConfig == undefined) return;

        if(this.generalConfig.twitch != undefined && this.generalConfig.twitch.channel != undefined) {
            Logger.log("Starting Twitch Bot for " + this.cfgfolder);
            this.twitchClient.start(callback);
            if(this.generalConfig.modCommand != undefined) {
                Logger.log("Starting Twitch ModBot for " + this.cfgfolder);
                this.modBot.start(callback);
            }
        }
        if(this.generalConfig.youtube != undefined && this.generalConfig.youtube.token != undefined) {
            Logger.log("Starting YouTube Bot for " + this.cfgfolder);
            this.youtubeClient.start(callback);
        }
    }

    public stop(callback:() => void) {
        if(this.twitchClient != undefined) this.twitchClient.stop(callback);
    }

}