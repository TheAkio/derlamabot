import {YouTubeChatPoll} from "../youtubechat/chatpoll";
import {GoogleOAuth} from "../youtubechat/oauth";
import {LamaClient} from "./lamaclient";

export class YouTubeWrapper extends YouTubeChatPoll implements LamaClient {

    private messageCallback:(senderDisplayName:string, senderIdName:string, isMod:boolean, isOwner:boolean, message:string) => void;

    constructor(lamaBotAuth:GoogleOAuth, userAuth:GoogleOAuth) {
        super(lamaBotAuth, userAuth);
    }

    protected onYouTubeMessage(data:any) {
        if(this.messageCallback == undefined) return;
        if(data.snippet.type == "textMessageEvent") {
            let message = data.snippet.displayMessage;
            let senderDisplayName = data.authorDetails.displayName;
            let senderId = data.authorDetails.channelId;
            let mod = data.authorDetails.isChatModerator;
            let owner = data.authorDetails.isChatOwner;
            this.messageCallback(senderDisplayName, senderId, mod, owner, message);
        }
    }

    public start(callback:() => void) {
        super.startPolling();
        callback();
    }

    public stop(callback:() => void) {
       super.stopPolling();
       callback();
    }

    public say(message:string) {
        let split = message.split("\n");
        let index = -1;
        let callback = () => {
            index++;
            if(split.length > index) {
                super.insert(split[index], callback);
            }
        }
        callback();
    }

    public setMessageCallback(callback:(senderDisplayName:string, senderId:string, isMod:boolean, isOwner:boolean, message:string) => void) {
        this.messageCallback = callback;
    }

    public getWrapper() {
        return "youtube";
    }

}