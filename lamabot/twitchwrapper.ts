import {TwitchIRC} from "../twitchchat/irc";
import {TwitchUser} from "../twitchchat/user";
import {TwitchMessage} from "../twitchchat/message";
import {LamaClient} from "./lamaclient";

export class TwitchWrapper extends TwitchIRC implements LamaClient {

    private channel:string;
    private messageCallback:(senderDisplayName:string, senderIdName:string, isMod:boolean, isOwner:boolean, message:string) => void;

    constructor(username:string, password:string, channel:string) {
        super(username, password, [channel]);
        this.channel = channel;
    }

    protected onTwitchMessage(user:TwitchUser, message:TwitchMessage) {
        if(this.messageCallback != undefined) this.messageCallback(user.getDisplayName(), message.getUsername().toLowerCase(), user.isMod(), this.channel.substr(1).toLowerCase() == message.getUsername().toLowerCase(),  message.getMessage());
    }

    public start(callback:() => void) {
        super.connect(callback);
    }

    public stop(callback:() => void) {
        super.disconnect(callback);
    }

    public say(message:string) {
        super.say(this.channel, message);
    }

    public setMessageCallback(callback:(senderDisplayName:string, senderId:string, isMod:boolean, isOwner:boolean, message:string) => void) {
        this.messageCallback = callback;
    }

    public getWrapper() {
        return "twitch";
    }

}