export interface LamaClient {
    
    start(callback:() => void):void;
    stop(callback:() => void):void;
    say(message:string):void;
    setMessageCallback(callback:(senderDisplayName:string, senderId:string, isMod:boolean, isOwner:boolean, message:string) => void):void;
    getWrapper():string;

}