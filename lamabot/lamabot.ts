import {LamaClient} from "./lamaclient";
import {_} from "../util/util";

declare function require(name:string):any;

const fs = require("fs");

export class LamaBot {

    private client:LamaClient;

    private cfgfolder:string;

    private generalConfig:any;
    private commandConfig:any;
    private messagesConfig:any;
    private data:any;
    private lastUse:{[key: string]: number};

    private running:boolean = false;

    constructor(client:LamaClient, cfgfolder:string, generalConfig:any) {
        this.client = client;
        this.cfgfolder = cfgfolder;
        this.generalConfig = generalConfig;
        this.lastUse = {};
        if(!fs.existsSync(this.cfgfolder + "/commands.json")) fs.writeFileSync(this.cfgfolder + "/commands.json", "{\"commands\":{}}");
        if(!fs.existsSync(this.cfgfolder + "/messages.json")) fs.writeFileSync(this.cfgfolder + "/messages.json", "{\"messages\":[]}");
        if(!fs.existsSync(this.cfgfolder + "/data.json")) fs.writeFileSync(this.cfgfolder + "/data.json", "{\"twitch_modplus\":[],\"youtube_modplus\":[]}");
        this.commandConfig = require("../" + this.cfgfolder + "/commands.json");
        this.messagesConfig = require("../" + this.cfgfolder + "/messages.json");
        this.data = require("../" + this.cfgfolder + "/data.json");
        this.client.setMessageCallback((senderDisplayName:string, senderId:string, isMod:boolean, isOwner:boolean, message:string) => {
            this.onMessage(senderDisplayName, senderId, isMod, isOwner, message);
        });
    }

    private onMessage(senderDisplayName:string, senderId:string, isMod:boolean, isOwner:boolean, message:string) {
        if(_.startsWith(message, "!")) {
            this.processCommand(senderDisplayName, senderId, isMod, isOwner, message);
        }
        else if(this.running && !isMod && !isOwner) {
            let msg = message.toLowerCase();
            let search = this.messagesConfig.messages;
            for(let i = 0; i < search.length; i++) {
                let data = search[i];
                if(msg.indexOf(data.mainKey) > -1 && (this.canUse(data.mainKey, data.cooldown))) {
                    for(let j = 0; j < data.alsoRequired.length; j++) {
                        if(msg.indexOf(data.alsoRequired[j]) > -1) {
                            this.use(data.mainKey);
                            this.client.say(data.message.replace("%0", senderDisplayName));
                            break;
                        }
                    }
                }
            }
        }
    }

    private processCommand(senderDisplayName:string, senderId:string, isMod:boolean, isOwner:boolean, message:string) {
        let split = message.split(" ");
        let command = split[0].toLowerCase();
        if(command == "!modplus" && split.length > 1 && ((isMod && this.isModPlus(senderId)) || isOwner)) {
            let user = this.client.getWrapper() == "twitch" ? split[1].toLowerCase() : split[1];
            if(!this.isModPlus(user)) {
                this.addModPlus(user);
                this.saveData();
                this.client.say(user + " ist nun Mod+");
            }
            else {
                this.removeModPlus(user);
                this.saveData();
                this.client.say(user + " ist nun kein Mod+ mehr");
            }
        }
        else if(command == "!start" && ((isMod && this.isModPlus(senderId)) || isOwner) && !this.running) {
            this.running = true;
            this.client.say("Bot gestartet!");
        }
        else if(command == "!stop" && ((isMod && this.isModPlus(senderId)) || isOwner) && this.running) {
            this.running = false;
            this.client.say("Bot gestoppt!");
        }
        else if(this.running) {
            let useCmd = command.substr(1);
            let commandData = this.commandConfig.commands[useCmd];
            if(commandData == null) return;
            while(commandData.aliasOf != undefined) {
                useCmd = commandData.aliasOf;
                commandData = this.commandConfig.commands[commandData.aliasOf];
            }
            if(commandData != null && (this.canExecute(commandData, isMod, this.isModPlus(senderId)) || isOwner)) {
                if(this.useCommand(useCmd, commandData.cooldown)) {
                    if(commandData.args == undefined) {
                        if(commandData.message == undefined) return;
                        let message = commandData.message;
                        message = this.formatOutputMessage(message, senderDisplayName);
                        this.client.say(message);
                    }
                    else {
                        let dataArgs = commandData.args;
                        let showMessage = false;
                        if(split.length - 1 >= dataArgs.length) {
                            for(let i = 0; i < dataArgs.length; i++) {
                                let dataArg = dataArgs[i];
                                let arg = split[i + 1];
                                if(dataArg.processor == "condition") {
                                    let checkFunction:(argument:string, check:string) => boolean;
                                    switch(dataArg.function) {
                                        case "contains":
                                            checkFunction = _.contains;
                                            break;
                                        case "containsIC":
                                            checkFunction = _.containsIgnoreCase;
                                            break;
                                        case "equals":
                                            checkFunction = _.equals;
                                            break;
                                        case "equalsIC":
                                            checkFunction = _.equalsIgnoreCase;
                                            break;
                                    }
                                    if(checkFunction != null) {
                                        let conditions = dataArg.conditions;
                                        for(let key in conditions) {
                                            if(!conditions.hasOwnProperty(key)) continue;
                                            if(checkFunction(arg, key)) {
                                                showMessage = true;
                                                let commandOperations = conditions[key];
                                                if(commandOperations.set != undefined) {
                                                    let setOperations = commandOperations.set;
                                                    for(let j = 0; j < setOperations.length; j++) {
                                                        let operation = setOperations[j];
                                                        this.data[this.client.getWrapper() + "_" + operation.dataKey] = operation.dataValue;
                                                    }
                                                    this.saveData();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            if(commandData.message == undefined || !showMessage) return;
                            let message = commandData.message;
                            message = this.formatOutputMessage(message, senderDisplayName);
                            this.client.say(message);
                        }
                    }
                }
            }
        }
    }

    private formatOutputMessage(message:string, senderDisplayName:string) {
        if(senderDisplayName.length > 20) senderDisplayName = senderDisplayName.substr(0, 20);
        message = message.replace("%0", senderDisplayName);
        for(let key in this.data) {
            if(!this.data.hasOwnProperty(key)) continue;
            if(key == "modplus") continue;
            if(!_.startsWith(key, this.client.getWrapper())) continue;
            let replaceKey = key.replace(this.client.getWrapper() + "_", "");
            while(_.contains(message, "%" + replaceKey)) message = message.replace("%" + replaceKey, this.data[key]);
        }
        return message;
    }

    private canExecute(commandData:any, isMod:boolean, isModPlus:boolean) {
        let platform = this.client.getWrapper() == "twitch" && commandData.twitch || this.client.getWrapper() == "youtube" && commandData.youtube;
        let mod = commandData.requiresMod ? isMod : true;
        let modplus = commandData.requiresModPlus ? isModPlus : true;
        return platform && mod && modplus;
    }

     private useCommand(command:string, cooldown:number) {
        if(this.canUse(command, cooldown)) {
            this.use(command);
            return true;
        }
        return false;
    }

    private canUse(command:string, cooldown:number) {
        if(this.lastUse[command] == undefined) return true;
        let time = this.lastUse[command];
        return time + cooldown < new Date().getTime();
    }

    private use(command:string) {
        this.lastUse[command] = new Date().getTime();
    }

    private saveData() {
        fs.writeFileSync(this.cfgfolder + "/data.json", JSON.stringify(this.data));
    }

    private addModPlus(senderId:string) {
        this.data[this.client.getWrapper() + "_modplus"].push(senderId);
    }

    public isModPlus(senderId:string) {
        for(let i = 0; i < this.data[this.client.getWrapper() + "_modplus"].length; i++) {
            if(this.data[this.client.getWrapper() + "_modplus"][i] == senderId) return true;
        }
        return false;
    }

    public isRunning() {
        return this.running;
    }

    private removeModPlus(senderId:string) {
        let newModPlus = [];
        for(let i = 0; i < this.data[this.client.getWrapper() + "_modplus"].length; i++) {
            if(this.data[this.client.getWrapper() + "_modplus"][i] != senderId) newModPlus.push(this.data[this.client.getWrapper() + "_modplus"][i]);
        }
        this.data[this.client.getWrapper() + "_modplus"] = newModPlus;
    }

}