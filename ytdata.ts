import {GoogleOAuth} from "./youtubechat/oauth";

declare var process:any;

if(process.argv.length > 2) {
    if(process.argv[2] == "url") {
        console.log(new GoogleOAuth().generateURL());
    }
    else if(process.argv[2] == "token" && process.argv.length > 3) {
        let auth = new GoogleOAuth();
        auth.setCredentials(process.argv[3], (err:any) => {
            if(err) {
                console.log("ERROR: " + err);
            }
            else {
                console.log(auth.getTokens());
            }
        });
    }
}